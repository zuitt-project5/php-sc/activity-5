<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>s5: Activity 5</title>
</head>
<body>
	<?php session_start(); ?>
	
	<?php if(!isset($_SESSION["email"])): ?>

		<h1>Login Form</h1>
		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="login">
			<label>Email:</label>
			<input type="email" name="email" required />

			<label>Password:</label>
			<input type="password" name="password" required />

			<button type="submit">Login</button>
		</form>
		
		<?php if(isset($_SESSION['error_message'])): ?>
			<p><?= $_SESSION['error_message']; ?></p>
			<?php unset($_SESSION['error_message']) ?>
		<?php endif; ?>

	<?php else: ?>
		<h1>Logged In</h1>
		<p>Hi, <?= $_SESSION["email"]; ?>. Thanks for Logging In.</p>

		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="logout">
			<button type="submit">Logout</button>
		</form>
	<?php endif; ?>

</body>
</html>